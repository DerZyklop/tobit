PwView = require './base/pw_view'
application = require 'application'
logger = require 'logger'
session = require 'session'
store = require 'store'
formValues = require './../models/formvalues_model'

module.exports = class SearchForm3View extends PwView
  template: require './templates/lost_searchForm3'
  name: 'SearchForm3View'
  confirmOnBack: true

  formValues: new formValues
    'anlagennr': ''
    'mastnr': ''
    'technischerplatz': ''
    'treffer': false
    'switchablefieldtype': ''

  events:
    'click #back': 'navigateBack'
    'click #next': 'navigateNext'

    'click #trefferanzahl-btn': 'performTrefferBtn'

    'keyup input:not([type=hidden])': "setFormValues"
    'click input:not("#mastnr")': (e) ->
      if !@$("#mastnr").val()?.length
        @formValues.set('mastnr','')
    'cut input:not([type=hidden])': "setFormValues"
    'reset input:not([type=hidden])': "setFormValues"

    'click #typeswitch': "switchType"


  initialize: (options) ->
    if not @isBlank(options?.pid)
      @pid = options.pid
      @mastnrExists = (options.assembler == "2")
      @assembler = options.assembler
      @objectCatId = options.objectCatId
      @categorieId = options.categorieId
      @searchFormNumber = options.searchFormNumber

      @formValues.set
        'anlagennr': session.get("anlagennr")
        'mastnr': session.get("mastnr")
        'technischerplatz': session.get("technischerplatz")
        'treffer': session.get("treffer")
        'switchablefieldtype': @formValues.get("switchablefieldtype") || "number"

      @formValues.on
        'change:anlagennr': =>
          @setInputsAvailability()
          @checkInput()
        'change:mastnr': =>
          @checkInput()
        'change:technischerplatz': =>
          @checkInput()
        'change:treffer': @setTreffer
        'change:switchablefieldtype': @showSwitchedType

      super options

      @resetSessionData()

  switchType: ->
    @formValues.set 'switchablefieldtype', (if @formValues.get("switchablefieldtype") is "number" then "text" else "number")

  showSwitchedType: =>
    newInputEl = $('<input>').attr('id', 'newmastnr')

    @newAttributes = new Backbone.Model()
    _.each @$('#mastnr')[0].attributes, (attr) =>
      @newAttributes.set attr.name, attr.value

    @newAttributes.set 'type', @formValues.get('switchablefieldtype')

    _.each @newAttributes.attributes, (value, key) =>
      newInputEl[0][key] = value

    @$('#mastnr').after(newInputEl).remove()





  navigateBack: ->
    # Its important that formValues.clear() happens after resetSessionData()
    @resetSessionData()
    @formValues.clear 'silent': true
    @formValues.off()

    super
    @navigate("view?name=ObjectTypeCategoriesView&pid="+@pid+"&categorieId="+@categorieId)

  getRenderData: ->
    headtitle: ''
    headsubtitle: 'Suche anhand Equipment'
    resource: @model.resource
    mastnr_input: @mastnrExists
    anlagennr: @formValues.get("anlagennr")
    mastnr: @formValues.get("mastnr")
    treffer: @formValues.get("treffer")
    technischerplatz: @formValues.get("technischerplatz")
    switchablefieldtype: @formValues.get("switchablefieldtype")

  navigateNext: ->
    @formValues.off()
    super
    url =  "view?name=TechnicalPlacesView&"+@getUrlParams()
    url += "&categorieId=#{@categorieId}"
    url += "&pid=#{@pid}"
    url += "&searchFormNumber=#{@searchFormNumber}"
    url += "&assembler=#{@assembler}"

    @setSessionData()

    @navigate(url)

  setInputsAvailability: =>
    if @formValues.get('anlagennr').toString().length > 0
      @mkAvailable '#mastnr', true
      @mkAvailable '#technischerplatz', true
    else
      @formValues.set 'mastnr', ''
      @showChanges('mastnr')
      @formValues.set 'technischerplatz', ''
      @showChanges('technischerplatz')

      @mkAvailable '#mastnr', false
      @mkAvailable '#technischerplatz', false

  checkInput: =>
    @formValues.set 'treffer', false

    if @formValues.get('anlagennr').toString().length > 0
      @mkAvailable '#trefferanzahl-btn', true
    else
      @mkAvailable '#trefferanzahl-btn', false

  getUrlParams: ->
    url =    "objectCatId="+@objectCatId
    url +=   "&plantNumber="+@formValues.get('anlagennr')
    if @formValues.get('mastnr').toString().length > 0
      url += "&mastNumber="+@formValues.get('mastnr')
    url +=   "&description="+@formValues.get('technischerplatz')
    url +=   '&t='+Number(new Date())
    return url

  getScore: (callback = ->) =>
    # TODO: move this to pw_view when removed lostui

    url = "/enbw_wfm2/treffer?"+@getUrlParams()

    @performAjaxCall
      name: 'getScore'
      msg: 'Suchtreffer werden geladen...'
      errorMsg: 'Fehler: Trefferzahl konnte nicht geladen werden!'
      url: url
      success: (data) =>
        @formValues.set 'treffer', data.data.t
        callback()


  performTrefferBtn: (e) ->
    e.preventDefault()
    @disableContent()
    @mkAvailable '#trefferanzahl-btn', false
    @getScore =>
      @enableContent()

  showChanges: (selector) ->
    @$('#'+selector).val(@formValues.get(selector))

  setTreffer: ->
    @mkAvailable '#trefferanzahl-btn', !(@formValues.get('treffer') > 0)
    super


  afterRender: ->
    super

    session.set("objectCatId", @objectCatId)

    @mkAvailable '#trefferanzahl-btn', false
    @mkAvailable '#next', (@formValues.get('treffer').toString().length > 0)

    # TODO: Needs a better solution here
    setTimeout =>
      @mkAvailable '#next', (@formValues.get('treffer').toString().length > 0)
    , 300

    @setInputsAvailability()


  dispose: ->
    delete @formValues
    super
