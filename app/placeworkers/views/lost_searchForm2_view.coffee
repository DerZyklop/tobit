PwView = require './base/pw_view'
application = require 'application'
logger = require 'logger'
session = require 'session'
store = require 'store'

module.exports = class SearchForm2View extends PwView
  template: require './templates/lost_searchForm2'
  name: 'SearchForm2View'
  confirmOnBack: true
  alphacodeTable: new Backbone.Collection

  formValues: new Backbone.Model
    'umspannwerk': ''
    'alphacode': ''
    'treffer': false

  autocompleteData: (selector) ->
    @alphacodeTable.pluck('a')
  events:
    'click #back': 'navigateBack'
    # 'taphold #back': 'navigateBack'
    'click #next': 'navigateNext'

    'focus :input.autocomplete': "openAutocompleteInitial"

    'keydown input:not([type=hidden])': "handleEnter"
    'keyup input:not([type=hidden])': "setFormValues"
    'click input:not([type=hidden])': "setFormValues"
    'cut input:not([type=hidden])': "setFormValues"
    'reset input:not([type=hidden])': "setFormValues"


    # HOTFIX [begin]
    # See startAutocompleteInterval() in base/pw_view.coffee
    "focus #umspannwerk": "startAutocompleteInterval"
    # HOTFIX [end]


  initialize: (options) ->
    if not @isBlank(options?.pid)
      @pid = options.pid
      @objectCatId = options.objectCatId
      @categorieId = options.categorieId
      @searchFormNumber = options.searchFormNumber

      @formValues.set
        'umspannwerk': session.get("umspannwerk")
        'alphacode': session.get("alphacode")
        'treffer': session.get("treffer")
      @resetSessionData()

      @formValues.on
        'change:umspannwerk': @checkUmspannwerkInput
        'change:alphacode': @checkAlphaCode
        'change:treffer': @setTreffer

      super options

  handleEnter: (e) ->
    if e.keyCode == 13
      e.preventDefault()

  setFormValues: (e) ->
    if !(@$(e.target).val().length == 0)
      super
    else
      @showAutocomplete true

  openAutocompleteInitial: (e) ->
    el = @$(e.target)
    if el.val().length == 0

      el.val('a')
      el.trigger('keyup')
      el.val('')
      el.trigger('keyup')
      $('.autocomplete-suggestions').css('display','block')

  navigateBack: ->
    # Its important that formValues.clear() happens after resetSessionData()
    @resetSessionData()
    @formValues.clear 'silent': true
    @formValues.off()

    @stopAutocompleteInterval()
    super
    @navigate("view?name=ObjectTypeCategoriesView&pid="+@pid+"&categorieId="+@categorieId)

  navigateNext: ->
    @formValues.off()
    @stopAutocompleteInterval()
    super
    @setSessionData()
    @navigate("view?name=TechnicalPlacesView&objectCatId=#{@objectCatId}&categorieId=#{@categorieId}&pid=#{@pid}&searchFormNumber=#{@searchFormNumber}&alphacodeUmspannwerk="+@formValues.get('alphacode'))

  getRenderData: ->
    headtitle: ''
    headsubtitle: 'Suche anhand eines Umspannwerkes'
    resource: @model.resource
    umspannwerk: @formValues.get('umspannwerk')
    alphacode: @formValues.get('alphacode')
    treffer: @formValues.get('treffer')

  autocompleteDataExists: (selector) ->
    _.size(@alphacodeTable.pluck('a')) > 0

  valueExists: (value) ->
    @contains @alphacodeTable.pluck('a'), value

  getAlphaCodeOf: (value) ->
    if _.size(@alphacodeTable) > 0
      # Get first item where 'a' matches "el.val()"
      searchedItem = _.filter(@alphacodeTable.models, (item) ->
        if item.get('a')
          item.get('a').toLowerCase() == value.toLowerCase()
      )[0]

      @formValues.set 'alphacode', searchedItem.get('alphaCode')

  checkAlphaCode: =>
    if @formValues.get('alphacode').length > 0
      @formValues.set 'treffer', 1

  checkUmspannwerkInput: =>
    selector = 'umspannwerk'
    el = @$('#'+selector)

    if !@autocompleteIsSetHere(selector)
      @getUmspannwerkAutocomplete()

    if @formValues.hasChanged(selector)
      if el.val() && @autocompleteIsSetHere(selector) && @valueExists(el.val())

        @getAlphaCodeOf @$('#umspannwerk').val()

        @mkAvailable '#next', true

      else
        @formValues.set 'treffer', false

  getUmspannwerkAutocomplete: () ->
    selector = 'umspannwerk'
    el = @$(selector)

    if @objectCatId && @alphacodeTable.pluck('a')?.length && @autocompleteDataExists(selector)
      @setUmspannwerkAutocomplete()

    else
      url = '/enbw_wfm2/umspannwerke?objectCatId='+@objectCatId+'&t='+Number(new Date())
      @getAutocompleteStuff url, (data) =>
        @alphacodeTable.add data.data[@objectCatId]
        @setUmspannwerkAutocomplete()


  getMinCharParam: (autocompleteDataLength) ->
    Math.min(3, Math.floor(autocompleteDataLength / 100 / 2))


  setUmspannwerkAutocomplete: ->
    @setAutocomplete 'umspannwerk', @alphacodeTable.pluck('a'), @getMinCharParam(@alphacodeTable.pluck('a')?.length), =>
      @formValues.set
        'umspannwerk': @$('#umspannwerk').val()

    @$('form :input:enabled:visible:first').focus()

  afterRender: ->
    super

    session.set("objectCatId", @objectCatId)

    # TODO: Remove this timeout. Its a relict from lostui>init
    setTimeout =>
      @checkUmspannwerkInput()
    1000

  dispose: ->
    @stopAutocompleteInterval()
    @showAutocomplete(false)
    delete @formValues
    super
