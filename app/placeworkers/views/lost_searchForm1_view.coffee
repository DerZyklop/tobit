PwView = require './base/pw_view'
application = require 'application'
logger = require 'logger'
session = require 'session'
store = require 'store'

module.exports = class SearchForm1View extends PwView
  template: require './templates/lost_searchForm1'
  name: 'SearchForm1View'
  confirmOnBack: true
  autocompleteArray: []
  autocompleteData: (selector) ->
    @autocompleteArray[selector]
  events:
    # 'taphold #back': 'navigateBack'
    #"keyup #gemeinde": "doFilteredHelpval"

    #'focus :input:not([type=hidden])': "setFormValues"
    'keyup input:not([type=hidden])': "setFormValues"
    'click input:not([type=hidden])': "setFormValues"
    'cut input:not([type=hidden])': "setFormValues"
    'reset input:not([type=hidden])': "setFormValues"


    # HOTFIX [begin]
    # See startAutocompleteInterval() in base/pw_view.coffee
    "focus #gemeinde": "startAutocompleteInterval"
    "focus #ortsteil": "startAutocompleteInterval"
    # HOTFIX [end]

    'click #next': 'navigateNext'
    'click #back': 'navigateBack'

    'click #trefferanzahl-btn': 'performTrefferBtn'


  initialize: (options) ->
    if not @isBlank(options?.pid)
      @pid = options.pid
      @objectCatId = options.objectCatId
      @categorieId = options.categorieId
      @searchFormNumber = options.searchFormNumber
      super options

  navigateBack: ->
    @stopAutocompleteInterval()

    # Its important that formValues.clear() happens after resetSessionData()
    @resetSessionData()
    @formValues.clear 'silent': true
    @formValues.off()

    super
    @navigate("view?name=ObjectTypeCategoriesView&pid="+@pid+"&categorieId="+@categorieId)

  getRenderData: ->
    headtitle: ''
    headsubtitle: 'Suche anhand eines Standortes'
    resource: @model.resource

  navigateNext: ->
    @formValues.off()
    @stopAutocompleteInterval()
    super
    @setSessionData()
    @navigate("view?name=TechnicalPlacesView&objectCatId=#{@objectCatId}&categorieId=#{@categorieId}&pid=#{@pid}&searchFormNumber=#{@searchFormNumber}&alphacode="+session.get('alphacode')+"&description="+@$('#technischerplatz').val())

  getAlphaCode: (callback = ->) ->
    # Prevents unnecessary ajax-call when just technischerplatz changed
    if !(@formValues.get('alphacode').length > 0)

      url = "/enbw_wfm2/alphacode?"
      url += "g="+@formValues.get('gemeinde')+"&"
      url += "o="+@formValues.get('ortsteil')+"&"
      if @formValues.get('technischerplatz') != ''
        url += 'description='+@formValues.get('technischerplatz')+"&"
      url += "t="+Number(new Date())

      @performAjaxCall
        name: 'getAlphaCode'
        msg: 'Suchtreffer werden geladen...'
        errorMsg: 'Fehler: Daten konnten nicht geladen werden!'
        url: url
        success: (data) =>
          @formValues.set 'alphacode', data.data.alphaCode
        done: ->
          callback()

    else
      @getScore()

  getScore: =>
    url = "/enbw_wfm2/treffer?objectCatId="+session.get("objectCatId")+"&"
    url += "alphacode="+@formValues.get('alphacode')+"&"
    if @formValues.get('technischerplatz') != ''
      url += 'description='+@formValues.get('technischerplatz')+'&'
    url += 't='+Number(new Date())

    @performAjaxCall
      name: 'getScore'
      msg: 'Suchtreffer werden geladen...'
      errorMsg: 'Fehler: Trefferzahl konnte nicht geladen werden!'
      url: url
      success: (data) =>
        @formValues.set 'treffer': data.data.t


  performTrefferBtn: (e) ->
    e.preventDefault()
    @disableContent()
    @mkAvailable '#trefferanzahl-btn', false
    @autoTimeout.clear()
    @getAlphaCode =>
      @enableContent()

  checkInput: (selector) ->
    @showAutocomplete !@valueIsValid(selector) && @$('#'+selector).hasClass 'autocomplete'
    @formValues.set('treffer', false)
    @setInputsDisableStatus()

  valueIsValid: (selector) ->
    if selector == 'gemeinde' || selector == 'ortsteil'
      if @autocompleteArray?[selector]
        return @contains @autocompleteArray?[selector], @formValues.get(selector)

      else
        return false
    else
      return true

  checkGemeindeInput: =>
    selector = 'gemeinde'
    @formValues.set('alphacode', '')
    @checkInput(selector)

    if @valueIsValid(selector)
      @$('#ortsteil').focus()

    if @autocompleteArray?['ortsteil'] != ''
      @autocompleteArray?['ortsteil'] = undefined

    @performElement
      selector: selector
      url: '/enbw_wfm2/gemeinden?t='+Number(new Date())
      autocompleteMinChars: 3
      callback: =>
        @formValues.set
          'gemeinde': @$('#gemeinde').val()

        @checkOrtsteilInput()

  checkOrtsteilInput: =>
    selector = 'ortsteil'
    @formValues.set('alphacode', '')
    @checkInput(selector)

    if @valueIsValid(selector)
      @$('#technischerplatz').focus()
    else
      @formValues.set 'technischerplatz', ''

    @autoTimeout.clear()
    @performElement
      selector: selector
      url: '/enbw_wfm2/ortsteile?g='+@formValues.get('gemeinde')+'&t='+Number(new Date())
      callback: =>
        @$('.autocomplete-suggestions').hide()
        @formValues.set
          'ortsteil': @$('#ortsteil').val()

        @autoTimeout.set =>
          @getAlphaCode()



  autoTimeout: ( ->
    _timeout = false
    set = (callback = (->), delay = 2000) ->
      clear() if _timeout
      _timeout = setTimeout ->
        callback()
        clear()
      , delay
    clear = ->
      clearTimeout _timeout
      _timeout = false
    return {
      set: set
      clear: clear
    }
  )()



  checkTechnischerplatzInput: =>
    selector = 'technischerplatz'
    @checkInput(selector)

    @$('#'+selector).val @formValues.get(selector)

    if @formValues.get('technischerplatz').length > 0
      @autoTimeout.clear()
    else
      @autoTimeout.set =>
        @getAlphaCode()

  autocompleteDataExists: (selector) ->
    @autocompleteArray?[selector] != undefined && @autocompleteArray?[selector] != ''


  performElement: (params) ->
    selector = params?.selector
    url = params?.url
    previous_input = params?.previous_input
    callback = params?.callback

    autocompleteArray = @autocompleteArray?[selector]

    if @autocompleteIsSetHere(selector)

      inputMatches = @contains autocompleteArray, @formValues.get(selector)

      if inputMatches || @formValues.get(selector).length == 0
        @setInputsDisableStatus()

      if inputMatches
        if callback
          callback()
      else
        if !(@formValues.get 'treffer' == '-')
          @formValues.set 'treffer', false

        @mkAvailable '#trefferanzahl-btn', false
        @autoTimeout.clear()

    else

      if @autocompleteDataExists(selector) && @$('#'+selector).width()
        @setAutocomplete selector, @autocompleteArray?[selector], params.autocompleteMinChars, params.callback

      else

        @getAutocompleteStuff url, (data) =>
          @autocompleteArray[selector] = []
          @autocompleteArray[selector].push(resultList.a) for resultList in data.data

          @setAutocomplete params.selector, @autocompleteArray?[selector], params.autocompleteMinChars, params.callback


  getSessionData: ->
    setVal = (selector) =>
      @$('#'+selector).val( session.get(selector) )
      @formValues.set selector, session.get(selector),
        silent: true

    setVal('gemeinde')
    setVal('ortsteil')
    setVal('alphacode')
    setVal('technischerplatz')

    @formValues.set 'treffer', session.get('treffer')


  setInputsDisableStatus: ->
    @mkAvailable '#gemeinde', !(@formValues.get('ortsteil').length > 0)
    @mkAvailable '#ortsteil', @valueIsValid('gemeinde') && !((@formValues.get('gemeinde').length == 0))
    @mkAvailable '#technischerplatz', (@valueIsValid('ortsteil')) || (@formValues.get('technischerplatz').length != 0)
    @mkAvailable '#next', !(@formValues.get('ortsteil').length == 0)
    @mkAvailable '#trefferanzahl-btn', @valueIsValid('gemeinde') && @valueIsValid('ortsteil') && !(@formValues.get('treffer') > 0)

  setTreffer: ->
    @mkAvailable '#trefferanzahl-btn', !(@formValues.get('treffer') > 0)
    super


  afterRender: ->
    super

    @formValues = new Backbone.Model
      'gemeinde': ''
      'ortsteil': ''
      'technischerplatz': ''
      'alphacode': ''
      'treffer': false

    @formValues.on
      'change:gemeinde': @checkGemeindeInput
      'change:ortsteil': @checkOrtsteilInput
      'change:technischerplatz': @checkTechnischerplatzInput
      'change:alphacode': @getScore
      'change:treffer': @setTreffer

    session.set("objectCatId", @objectCatId)

    @mkAvailable '#trefferanzahl-btn', false
    @mkAvailable '#next', false

    @getSessionData()
    @resetSessionData()
    @setInputsDisableStatus()
    if !@$('#gemeinde').hasClass('ui-disabled')
      @checkGemeindeInput()

  dispose: ->
    @stopAutocompleteInterval()
    @showAutocomplete(false)
    delete @formValues
    super
