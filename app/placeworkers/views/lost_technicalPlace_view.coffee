PwView = require './base/pw_view'
logger = require 'logger'
store = require 'store'
nav = require 'navigation'
session = require 'session'

module.exports = class TechnicalPlaceView extends PwView
  template: require './templates/lost_technicalPlace'
  name: 'TechnicalPlaceView'
  events:
    'click #level_back': 'navigateLevelBack'
    # 'taphold #level_back': 'navigateLevelBack'
    'click #back': 'navigateBack'
    # 'taphold #back': 'navigateBack'
    'click #save': 'saveAndClose'
    # 'taphold #save': 'saveAndClose'
    'click .item': 'navigateNext'
    # 'taphold .item': 'navigateNext'

  initialize: (options) ->
    if not @isBlank(options?.pid)
      @pid = options?.pid

      @tplnr = decodeURIComponent options?.tplnr
      @equnr = decodeURIComponent options?.equnr
      @text = decodeURIComponent options?.text

      @alphacode = options.alphacode
      @alphacodeUmspannwerk = options.alphacodeUmspannwerk

      @categorieId = options.categorieId
      @objectCatId = options.objectCatId
      @searchFormNumber = options.searchFormNumber
      @assembler = options.assembler
      super options

  navigateBack: ->
    super
    @navigate("view?name=SearchForm"+@searchFormNumber+"View&pid="+@pid+"&searchFormNumber="+@searchFormNumber+"&objectCatId="+@objectCatId+"&categorieId="+@categorieId+"&assembler="+@assembler)

  navigateLevelBack: ->
    @disableContent()
    history = session.get('history')
    if history
     lastTP = history.pop()
     if lastTP
      nav.technischerPlatz
       tplnr: encodeURIComponent lastTP[0]
       equnr: encodeURIComponent lastTP[1]
       text: encodeURIComponent lastTP[2]
       alphacode: @alphacode
       alphacodeUmspannwerk: @alphacodeUmspannwerk
       pid: @pid
       categorieId: @categorieId
       objectCatId: @objectCatId
       searchFormNumber: @searchFormNumber
       assembler: @assembler
     else
      @navigate("view?name=TechnicalPlacesView&objectCatId=#{@objectCatId}&categorieId=#{@categorieId}&pid=#{@pid}&searchFormNumber=#{@searchFormNumber}&assembler=#{@assembler}&alphacode=#{@alphacode}&alphacodeUmspannwerk=#{@alphacodeUmspannwerk}&plantNumber="+session.get('anlagennr')+"&mastNumber="+session.get('mastnr'))
    else
     @navigate("view?name=TechnicalPlacesView&objectCatId=#{@objectCatId}&categorieId=#{@categorieId}&pid=#{@pid}&searchFormNumber=#{@searchFormNumber}&assembler=#{@assembler}&alphacode=#{@alphacode}&alphacodeUmspannwerk=#{@alphacodeUmspannwerk}&plantNumber="+session.get('anlagennr')+"&mastNumber="+session.get('mastnr'))

  saveAndClose: () ->
    @disableContent()
    @setTPResult(@tplnr,@equnr,@pid)

  getViewData: (options) ->
    @model.technischerPlatzChilds.fetch
     data:
      tplnr: @tplnr
      equnr: @equnr
      text: @text
     success: (collection, response) =>
      @handleViewDataSuccess(options)
     error: (collection, response) =>
      @handleViewDataError(collection, response)

  getRenderData: ->
    headtitle: ''
    headsubtitle: 'Technische Plätze'
    resource: @model.resource
    pid: @pid
    tplnr: @tplnr
    equnr: @equnr
    text: @text
    technischerPlatzChilds: @model.technischerPlatzChilds.models

  navigateNext: (event) ->
    super
    history = session.get('history')
    if history
     entry = new Array()
     entry.push(@tplnr)
     entry.push(@equnr)
     entry.push(@text)
     history.push(entry)
    else
     history = new Array()
     entry = new Array()
     entry.push(@tplnr)
     entry.push(@equnr)
     entry.push(@text)
     history.push(entry)

    session.set('history', history);

    nav.technischerPlatz
      tplnr: encodeURIComponent event.currentTarget.getAttribute "data-tplnr"
      equnr: encodeURIComponent event.currentTarget.getAttribute "data-equnr"
      text: encodeURIComponent event.currentTarget.getAttribute "data-text"
      pid: @pid
      alphacode: @alphacode
      alphacodeUmspannwerk: @alphacodeUmspannwerk
      categorieId: @categorieId
      objectCatId: @objectCatId
      searchFormNumber: @searchFormNumber
      assembler: @assembler

  afterRender: ->
    super
    @query(".item:first")?.addClass('order_list_hover') # highlight first item
