PwView = require './base/pw_view'
logger = require 'logger'
store = require 'store'
nav = require 'navigation'
session = require 'session'

module.exports = class TechnicalPlacesView extends PwView
  template: require './templates/lost_technicalPlaces'
  name: 'TechnicalPlacesView'
  mask: 'DD.MM.YYYY'
  day: 0
  events:
    'click #back': 'navigateBack'
    # 'taphold #back': 'navigateBack'
    'click .item': 'navigateNext'
    # 'taphold .item': 'navigateNext'

  initialize: (options) ->
    if not @isBlank(options?.pid)
      @pid = options.pid

      @alphacode = options.alphacode
      @alphacodeUmspannwerk = options.alphacodeUmspannwerk

      @plantNumber = options.plantNumber
      @mastNumber = options.mastNumber
      @description = options.description

      @categorieId = options.categorieId
      @objectCatId = options.objectCatId
      @searchFormNumber = options.searchFormNumber
      @assembler = options.assembler
      session.remove('history')
      super options

  navigateBack: ->
    super
    @navigate("view?name=SearchForm"+@searchFormNumber+"View&pid="+@pid+"&searchFormNumber="+@searchFormNumber+"&objectCatId="+@objectCatId+"&categorieId="+@categorieId+"&assembler="+@assembler)

  getViewData: (options) ->
    @model.technischePlaetze.fetch
     data:
      alphacode: @alphacode
      alphacodeUmspannwerk: @alphacodeUmspannwerk
      plantNumber: @plantNumber
      mastNumber: @mastNumber
      description: @description
      objectCatId: @objectCatId
     success: (collection, response) =>
      @handleViewDataSuccess(options)
     error: (collection, response) =>
      @handleViewDataError(collection, response)

  getRenderData: ->
    headtitle: ''
    headsubtitle: 'Technische Plätze'
    resource: @model.resource
    pid: @pid
    searchFormNumber: @searchFormNumber
    assembler: @assembler
    categorieId: @categorieId
    objectCatId: @objectCatId
    alphacode: @alphacode
    alphacodeUmspannwerk: @alphacodeUmspannwerk
    technischePlaetze: @model.technischePlaetze.models

  navigateNext: (event) ->
    super
    nav.technischerPlatz
      tplnr: encodeURIComponent event.currentTarget.getAttribute "data-tplnr"
      equnr: encodeURIComponent event.currentTarget.getAttribute "data-equnr"
      text: encodeURIComponent event.currentTarget.getAttribute "data-text"
      pid: @pid
      alphacode: @alphacode
      alphacodeUmspannwerk: @alphacodeUmspannwerk
      categorieId: @categorieId
      objectCatId: @objectCatId
      searchFormNumber: @searchFormNumber
      assembler: @assembler

  afterRender: ->
    super
    @query(".item:first")?.addClass('order_list_hover') # highlight first item
 # highlight first item
