###
# lost_view documentation #

This provides some additional functions for views that are made by
PlaceWorkers.

- Autocomplete

###

#View = require "/views/base/view"
# TODO: Get rid of lostui
TempView = require "/placeworkers/lostui"

# application = require 'application'
# nav = require 'navigation'
# logger = require 'logger'
ui = require 'ui'
session = require 'session'
# store = require 'store'

module.exports = class PwView extends TempView

  # This is for sending logs from BB to Eclipse
  ideConsoleLog = (msg) ->
    jQuery.ajax
      url: 'http://localhost:8472/enbw_wfm2/log/'
      dataType: 'json'
      async: true
      type: 'POST'
      data: { 'classifier': 'PwView', 'severity': 'INFO', 'message': msg }


  initialize: (options) ->
    _.extend @events,
      'keydown input': (event) =>
        @handleMaxLength event
        @handleKey event
      'keyup input[type="number"]': "avoidNaN"

    if not @isBlank(@pid)
      super options

    @$('form :input:enabled:visible:first').focus()

  avoidNaN: (event) ->
    if event.keyCode == 8 && !@$(event.target).val().length
      @$(event.target).val(0)

  navigateBack: ->
    # The childview should contain @navigateBack
    @disableContent()

  navigateNext: ->
    # The childview should contain @navigateNext
    @disableContent()

  parseInt: (value) ->
    parseInt(value, 10)

  inputReachedMaxLength = (event) ->
    max = @parseInt(event.target.max)
    val = @parseInt(event.target.value)
    if (max) && (val >= max) then max else false


  handleMaxLength: (event) =>

    el = event.target

    sliceToMax = (event) ->
      # This makes the "maxlength"-atribute on input[type="number"] work on BB's.
      max = inputReachedMaxLength(event)
      if max
        el = @$(event.target)
        el.val( el.val().slice(0,String(max).length) )
        el.trigger('keyup')

    valueChanged = (value) =>
      return ( value != @formValues.get($(el).attr('id')) )

    #ALTERNATIVE
    userTriedToAddAChar = =>
      return inputReachedMaxLength(event) && ( @$(el).attr('type') != 'number' || valueChanged(@$(el).val()) )

    showLenghNotification = =>
      @setOverlay('Maximal '+inputReachedMaxLength(event))

    notifyIfNecessary = =>
      if userTriedToAddAChar()
        showLenghNotification()
      @$(el).off 'keyup', notifyIfNecessary
      sliceToMax(event)

    # If the maxlength is reached, the bomb gets primed.
    # If the trigger was e.g. a function-key, the bomb get defused.
    if (event.keyCode != undefined) && (event.target.max)
      if inputReachedMaxLength(event)
        @$(el).on 'keyup', notifyIfNecessary
      else
        @$(el).off 'keyup', notifyIfNecessary


  setAutocomplete: (selector, autocompleteArray, minChars = 0, callback) ->
    @$('input.autocomplete').removeClass('autocomplete')
    jQuery('.autocomplete-suggestions').remove()
    @$('input#'+selector).addClass('autocomplete')

    @$('#'+selector).autocomplete
      lookup: autocompleteArray
      minChars: minChars
      onSelect: =>
        if callback then callback()


  autocompleteIsSetHere: (selector) ->
    @$('#'+selector).hasClass('autocomplete')

  showAutocomplete: (param) ->
    if param
      $('.autocomplete-suggestions').show()
    else
      $('.autocomplete-suggestions:visible').hide()

  setFormValues: (event = false) ->

    convertToNumIfPossible = (param) ->
      if isNaN(@parseInt(param)) then param else @parseInt(param)

    getElement = (event) ->
      if event then @$(event.target) else @$('input:focus')

    el = getElement(event)

    @formValues.set "#{el.attr('id')}", convertToNumIfPossible(el.val())

  getAutocompleteStuff: (url, callback) ->
    @performAjaxCall
      name: 'getAutocompleteStuff'
      msg: 'Vorschläge werden geladen...'
      errorMsg: 'Fehler: Daten für konnten nicht geladen werden!'
      url: url
      success: ( data ) =>
        if callback
          callback(data)


  setTreffer: =>

    mkHumanReadableFrom = (value) ->
      if value == false || value == ''
        value = '-'
      else if typeof value == 'string' && value != '-'
        value = @parseInt(value)
      else if (isNaN value) || value < 0
        value = 'error'

      return value

    @$('#treffer').html mkHumanReadableFrom @formValues.get('treffer')

    @mkAvailable '#next', (@formValues.get('treffer') > 0)


  mkAvailable: (selector, status = true) ->
    if status
      @enable selector
    else
      @disable selector


  enable: (selector) ->
    # Makes disable also work with jQuery-ui-buttons, not just with input-elements.
    el = @query(selector)
    if el.prop("tagName")?.toLowerCase() == 'button'
      el.button().button('enable')
    else
      el.textinput().textinput('enable')

  disable: (selector) ->
    # Makes disable also work with jQuery-ui-buttons, not just with input-elements.
    el = @query(selector)
    if el.prop("tagName")?.toLowerCase() == 'button'
      el.button().button('disable')
    else
      el.textinput().textinput('disable')

  contains: (array, value) ->
    # This Function is the same like _.contains(...) but it's not case sensitive
    result = false
    for item in array
      if item.toString().toLowerCase() == value.toString().toLowerCase()
        result = true
    result

  # TODO: improve
  startAutocompleteInterval: (e) ->
    if @autocompleteIntervalHandle
      @stopAutocompleteInterval()

    @autocompleteIntervalHandle = setInterval =>

      if !@$(':focus').length || @$(':focus').prop('tagName')?.toLowerCase() != 'input'
        if !@contains(@autocompleteData(e.target.id), e.target.value)
          @$('.autocomplete').trigger 'keyup'
      else
        @$('input:focus').trigger 'keyup'

    , 1000

  stopAutocompleteInterval: ->
    clearInterval @autocompleteIntervalHandle
    @autocompleteIntervalHandle = false
  # HOTFIX [end]

  setSingleSessionData: (name) ->
    session.set(name, @formValues.get(name))
  setSessionData: ->
    _.each @formValues.toJSON(), (value, key) =>
      @setSingleSessionData(key)

  resetSingleSessionData: (name) ->
    session.set(name, '')
  resetSessionData: ->
    _.each @formValues.toJSON(), (value, key) =>
      @resetSingleSessionData(key)










