session = require 'session'
nav = require 'navigation'
environment = require 'environment'
ui = require 'ui'

View = require "/views/base/view"

module.exports = class Lostui extends View


  # AJAX-Stuff [BEGIN]

  performAjaxCall: (params) ->

    runningajaxcall =
      set: (ajaxCallName, status) ->
        if status
          jQuery(@el).addClass 'runningajaxcall-'+ajaxCallName
        else
          jQuery(@el).removeClass 'runningajaxcall-'+ajaxCallName
      get: (ajaxCallName) ->
        if jQuery(@el).hasClass 'runningajaxcall-'+ajaxCallName
          return true
        else
          return false
    ajaxBeforeSend = (params, xhr) ->
      xhr.overrideMimeType("text/plain; charset=UTF-8")
      runningajaxcall.set params?.name, true
    ajaxSuccess = (params) =>
      runningajaxcall.set params?.name, false
      @removeOverlay(params?.msg)
    ajaxError = (params, request) ->
      @setOverlay(params?.errorMsg)
      setTimeout =>
        @removeOverlay(params?.errorMsg)
        runningajaxcall.set params?.name, false
      , 1500

    if !runningajaxcall.get params.name

      @setOverlay(params?.msg)

      setTimeout =>
        jQuery.ajax
          url: 'http://localhost:8472'+params.url
          dataType: 'json'
          async: false

          beforeSend: ( xhr ) =>
            ajaxBeforeSend(params, xhr)

          success: ( data ) =>
            ajaxSuccess(params)
            params.success(data)

          complete: =>

          error: (request) =>
            ajaxError(params, request)

      , 300

  # AJAX-Stuff [END]


  # Misc [BEGIN]

  setTPResult: (tplnr, equnr, pid) ->
    url = "/enbw_wfm2/tpResult?tplnr="+tplnr+"&equnr="+equnr
    url += "&t="+Number(new Date())

    @performAjaxCall
      name: 'setTPResult'
      msg: 'Speichere Auswahl...'
      errorMsg: 'Fehler: Auswahl konnte nicht gespeichert werden!'
      url: url
      success: (data) =>
        @clearLostSession()
        session.set("lost_tplnr", data.data.tplnr)
        session.set("lost_eq_text",  data.data.eq_text)
        session.set("lost_tp_text",  data.data.tp_text)
        session.set("lost_equnr", data.data.equnr)
        session.set("lost_arbpl", data.data.arbpl)
        session.set("lost_btserv", data.data.btserv)
        session.set("lost_werks", data.data.werks)
        session.set("lost_tplma", data.data.tplma)
        session.set("lost_eqart", data.data.eqart)
        session.set("lost_besitzer", data.data.besitzer)
        session.set("lost_lg", data.data.lg)
        session.set("lost_br", data.data.br)
        session.set("lost_rbnr", data.data.rbnr)

        #nav.goto("view?name=EinsatzlisteView&day=0&einsatzId=100")
        nav.lostResult(
          pid: pid
          lost_tplnr : data.data.tplnr
          lost_eq_text : data.data.eq_text
          lost_tp_text : data.data.tp_text
          lost_equnr : data.data.equnr
          lost_arbpl : data.data.arbpl
          lost_btserv : data.data.btserv
          lost_werks : data.data.werks
          lost_tplma : data.data.tplma
          lost_eqart : data.data.eqart
          lost_besitzer : data.data.besitzer
          lost_lg : data.data.lg
          lost_br : data.data.br
          lost_rbnr : data.data.rbnr
        )




  clearLostSession: ->
    items = [
      "lost_tplnr"
      "lost_eq_text"
      "lost_tp_text"
      "lost_equnr"
      "lost_arbpl"
      "lost_btserv"
      "lost_werks"
      "lost_tplma"
      "lost_eqart"
      "lost_besitzer"
      "lost_lg"
      "lost_br"
      "lost_rbnr"
      "historySteps"
      "history"
      "umspannwerk"
      "objectCatId"
      "alphacode"
      "treffer"
      "mastnr_input"
      "anlagennr"
      "mastnr"
      "technischerplatz"
      "gemeinde"
      "ortsteil"
    ]
    _.each items, (item) ->
      session.remove(item)

  # Misc [END]


  # Withdraw focus from the active element to prevent further user-input
  keyLogger =
    _el: false
    set: ->
      activeEl = document.activeElement
      if activeEl.tagName.toLowerCase() == 'input'
        @_el = document.activeElement
        jQuery(@_el).blur()
    remove: ->
      if @_el
        jQuery(@_el).focus()
        @_el = false




  fuseObject = {}

  setOverlay: (msg = 'Bitte warten...') =>

    # overlay shouldn't disappear when a new setOverlay comes in
    if _.size(fuseObject)
      _.each fuseObject, (value, key) ->
        clearTimeout fuseObject[key]
        delete fuseObject[key]

    if jQuery(@el).length > 0 # this checks if DOM is ready
      keyLogger.set()
      @disableContent()
      ui.showPageLoadingMsgCustom(msg)
      fuseObject[msg] = setTimeout =>
        @removeOverlay(msg)
      , 1500


  removeOverlay: (msg) =>
    fuseObject[msg] = setTimeout =>
      keyLogger.remove()
      @enableContent()
      ui.hidePageLoadingMsg()
    , 200
