phantom.casperPath = 'tools/casperjs'
phantom.injectJs "#{phantom.casperPath}/bin/bootstrap.js"

screenname = 's1'
pixelratio = 1.5
viewport =
  width: Math.round(480 / pixelratio)
  height: Math.round(640 / pixelratio)

casper = require('casper').create
  logLevel: 'debug'
  #verbose: true
  viewportSize: viewport

pw = require('./app/placeworkers/tests/functional/pw_TestsHelper.coffee')
pw = new pw(casper, viewport, screenname)

link = 'http://localhost:8472/app.html#view?name=SearchForm1View&pid=42&searchFormNumber=1&objectCatId=102&categorieId=2&assembler=undefined'

### Start ###
casper.start()

### Check the existence of all the Key-Elements on the Page ###
casper.thenOpen link, ->
  @echo "\nchecking elements of #{@getCurrentUrl()}:\n","info"
  pw.makeCapture()
  pw.assertExists ['#gemeinde','#ortsteil','#technischerplatz']

casper.wait 500, ->
  @test.assertEval (->
    !jQuery('[data-role="content"]').hasClass('ui-disabled'))
  , 'content is enabled'
  pw.makeCapture()

  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"
  @echo "Seite ist aufgebaut","info"
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"

casper.then ->
  pw.assertElementsStatus [{
      selector: '#gemeinde',
      enabled: true
    },{
      selector: '#ortsteil',
      enabled: false
    },{
      selector: '#technischerplatz',
      enabled: false
    }]

  pw.checkSuggestions()

casper.thenClick '#gemeinde', ->
  @fill 'form', {
    'gemeinde': 'BeRl'
  }, false
  @test.assertField 'gemeinde', 'BeRl', '#gemeinde enthält "BeRl"'
  @evaluate pw.keyup, ['#gemeinde']

  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"
  @echo "Filled #gemeinde with BeRl","info"
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"

casper.then ->
  @test.assertVisible '.autocomplete-suggestions', '[.autocomplete-suggestions] is visible'
  @test.assert @evaluate(pw.getSuggestionsAmount) > 0, 'Es gibt ein paar autocomplete-suggestions'
  pw.makeCapture()

casper.thenClick '.autocomplete-suggestion:first-child', ->
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"
  @echo "Clicked on first suggestion","info"
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"

  @test.assertNotVisible '.autocomplete-suggestions'

casper.then ->
  pw.assertElementsStatus [{
      selector: '#gemeinde',
      enabled: true
    },{
      selector: '#ortsteil',
      enabled: true
    },{
      selector: '#technischerplatz',
      enabled: false
    }]

casper.thenClick '#ortsteil', ->
  @fill 'form', {
    'ortsteil': 'wedd'
  }, false
  @test.assertField 'ortsteil', 'wedd', '#ortsteil enthält "wedd"'
  @evaluate pw.keyup, ['#ortsteil']

  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"
  @echo "Filled #ortsteil with wedd","info"
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"

casper.then ->
  @test.assertVisible '.autocomplete-suggestions', '[.autocomplete-suggestions] is visible'
  pw.checkSuggestions()
  @test.assert @evaluate(pw.getSuggestionsAmount) > 0, 'Es gibt ein paar autocomplete-suggestions'

  @test.assertEval ->
    jQuery('#treffer').html() == '-'

  pw.makeCapture()

  pw.assertElementsStatus [{
      selector: '#gemeinde',
      enabled: false
    },{
      selector: '#ortsteil',
      enabled: true
    },{
      selector: '#technischerplatz',
      enabled: false
    }]

  @evaluate ->
    jQuery('.autocomplete-suggestion:first-child').click()

casper.wait 1000, ->
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"
  @echo "Erste suggestion geklickt","info"
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"

  pw.makeCapture()
  @test.assertEvalEquals pw.assertTrefferBtnIsDisabled, false, '#trefferanzahl-btn ist enabled'
  @test.assertEval (->
    jQuery('#treffer').html() == '-')
  , 'treffer wurden noch nicht gesetzt'
  @test.assertEval (->
    !jQuery('[data-role="content"]').hasClass('ui-disabled'))
  , 'content is enabled'

casper.wait 1500, ->
  @test.assertEvalEquals pw.assertTrefferBtnIsDisabled, true, '#trefferanzahl-btn ist disabled'
  @test.assertEval (->
    jQuery('#treffer').html() != '-')
  , 'treffer wurden gesetzt'

casper.wait 500, ->

  @test.assertNotVisible '.autocomplete-suggestions', '[.autocomplete-suggestions] are gone'

  pw.assertElementsStatus [{
      selector: '#gemeinde',
      enabled: false
    },{
      selector: '#ortsteil',
      enabled: true
    },{
      selector: '#technischerplatz',
      enabled: true
    }]

  pw.makeCapture()

casper.then ->
  @fill 'form', {
    'technischerplatz': 'FooBar'
  }, false
  @test.assertField 'technischerplatz', 'FooBar', '#technischerplatz enthält "FooBar"'
  @evaluate pw.keyup, ['#technischerplatz']

  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"
  @echo "Filled #technischerplatz with FooBar","info"
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"

casper.thenClick '#trefferanzahl-btn', ->
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"
  @echo "Clicked #trefferanzahl-btn","info"
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"

casper.wait 500, ->

  pw.assertElementsStatus [{
      selector: '#gemeinde',
      enabled: false
    },{
      selector: '#ortsteil',
      enabled: true
    },{
      selector: '#technischerplatz',
      enabled: true
    }]

casper.thenClick '#next', ->
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"
  @echo "Clicked #next","info"
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"

casper.thenClick '#back', ->
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"
  @echo "Clicked #back","info"
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"

casper.then ->
  pw.makeCapture()
  @test.assertField 'gemeinde', 'BERLIN', '#gemeinde enthält "BERLIN"'
  @test.assertField 'ortsteil', 'WEDDING', '#ortsteil enthält "WEDDING"'
  @test.assertField 'technischerplatz', 'FooBar', '#technischerplatz enthält "FooBar"'

  pw.assertElementsStatus [{
      selector: '#gemeinde',
      enabled: false
    },{
      selector: '#ortsteil',
      enabled: true
    },{
      selector: '#technischerplatz',
      enabled: true
    }]

  @test.assertEval (->
    jQuery('#treffer').html() != '-')
  , 'treffer wurden gesetzt'

  pw.makeCapture()


# Nach un nach die einträge wieder entfernen

casper.then ->
  @fill 'form', {
    'technischerplatz': ''
  }, false
  @test.assertField 'technischerplatz', '', '#technischerplatz enthält nichts'
  @evaluate pw.keyup, ['#technischerplatz']

  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"
  @echo "Cleared #technischerplatz","info"
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"

  @fill 'form', {
    'technischerplatz': 'Foo'
  }, false

casper.then ->
  @test.assertField 'technischerplatz', 'Foo', '#technischerplatz enthält "Foo"'
  @evaluate pw.keyup, ['#technischerplatz']

  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"
  @echo "Filled #technischerplatz with FooBar","info"
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"

casper.thenClick '#trefferanzahl-btn', ->
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"
  @echo "Clicked #trefferanzahl-btn","info"
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"

  @fill 'form', {
    'ortsteil': 'WEDDIN'
  }, false

casper.then ->
  @test.assertField 'ortsteil', 'WEDDIN', '#ortsteil enthält "WEDDIN"'
  @evaluate pw.keyup, ['#ortsteil']

  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"
  @echo "Filled #ortsteil with WEDDIN","info"
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"

casper.then ->
  @test.assertField 'technischerplatz', '', '#technischerplatz ist leer'
  @test.assertEval (->
    jQuery('#treffer').html() == '-')
  , '#treffer ist "-"'

  pw.assertElementsStatus [{
      selector: '#gemeinde',
      enabled: false
    },{
      selector: '#ortsteil',
      enabled: true
    },{
      selector: '#technischerplatz',
      enabled: false
    }]
  pw.makeCapture()

casper.then ->
  @fill 'form', {
    'ortsteil': ''
  }, false
  @test.assertField 'ortsteil', '', '#ortsteil enthält nichts'
  @evaluate pw.keyup, ['#ortsteil']

  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"
  @echo "Cleared #ortsteil","info"
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"

  @click '#gemeinde'

casper.wait 1000, ->
  pw.assertElementsStatus [{
      selector: '#gemeinde',
      enabled: true
    },{
      selector: '#ortsteil',
      enabled: true
    },{
      selector: '#technischerplatz',
      enabled: false
    }]
  pw.makeCapture()

casper.then ->
  @fill 'form', {
    'gemeinde': 'BERL'
  }, false
  @test.assertField 'gemeinde', 'BERL', '#gemeinde enthält "BERL"'
  @evaluate pw.keyup, ['#gemeinde']

  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"
  @echo "Filled #gemeinde with BERL","info"
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"


casper.then ->
  @fill 'form', {
    'gemeinde': 'BER'
  }, false
  @evaluate pw.keyup, ['#gemeinde']

  @test.assertVisible '.autocomplete-suggestions', '[.autocomplete-suggestions] is visible'
  pw.checkSuggestions()
  pw.assertElementsStatus [{
      selector: '#gemeinde',
      enabled: true
    },{
      selector: '#ortsteil',
      enabled: false
    },{
      selector: '#technischerplatz',
      enabled: false
    }]

casper.then ->
  @fill 'form', {
    'gemeinde': ''
  }, false
  @test.assertField 'gemeinde', '', '#gemeinde enthält nichts'
  @evaluate pw.keyup, ['#gemeinde']

  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"
  @echo "Cleared #gemeinde","info"
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"

casper.then ->
  pw.assertElementsStatus [{
      selector: '#gemeinde',
      enabled: true
    },{
      selector: '#ortsteil',
      enabled: false
    },{
      selector: '#technischerplatz',
      enabled: false
    }]


casper.then ->
  @fill 'form', {
    'gemeinde': 'BER'
  }, false
  @evaluate pw.keyup, ['#gemeinde']

  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"
  @echo "Filled #gemeinde with BER","info"
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"

casper.then ->
  @test.assertEval ->
    jQuery('.autocomplete-suggestions').css('width').replace('px', '') > 50
  , 'Autocomplete ist breiter als 50px'

casper.thenClick '#back', ->
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"
  @echo "Clicked #back","info"
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"

casper.thenClick '#ojectkat_102', ->
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"
  @echo "Clicked #ojectkat_102","info"
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"

casper.then ->
  @fill 'form', {
    'gemeinde': 'BER'
  }, false
  @evaluate pw.keyup, ['#gemeinde']

casper.then ->
  @test.assertEval ->
    jQuery('.autocomplete-suggestions').css('width').replace('px', '') > 50
  , 'Autocomplete ist breiter als 50px'


casper.run ->
  @echo 'finished!\n',"info"

  @test.done()
  @exit()
