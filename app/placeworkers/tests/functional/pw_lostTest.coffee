phantom.casperPath = 'tools/casperjs'
phantom.injectJs "#{phantom.casperPath}/bin/bootstrap.js"

screenname = 's2'
pixelratio = 1.5
viewport =
  width: Math.round(480 / pixelratio)
  height: Math.round(640 / pixelratio)

casper = require('casper').create
  logLevel: 'debug'
  #verbose: true
  viewportSize: viewport

pw = require('./app/placeworkers/tests/functional/pw_TestsHelper.coffee')
pw = new pw(casper, viewport, screenname)



getHtml = ->
  jQuery('body').html()

getNrOfListItems = ->
  jQuery('.order_list').find('li').length

clickFirstListItemHref = ->
  item = jQuery('.order_list').find('li')[0]
  jQuery(item).children('a').click()




link = 'http://localhost:8472/app.html#view?name=CategoriesView&pid=42'

### Start ###
casper.start()

### Check the existence of all the Key-Elements on the Page ###
casper.thenOpen link, ->
  @log "\n===============================================================\n","info"
  @echo "checking elements of #{@getCurrentUrl()}:\n","info"

  keyElements = ['.order_list','#back']
  for elem in keyElements
    @test.assertExists elem, "- The [#{elem}] exists"

  @test.assertEvalEquals getNrOfListItems, 3, '- Found three list-items'

casper.thenClick '.order_list li a', ->

  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"
  @echo "Erstes listen-element angeklickt","info"
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"

  @echo "#{@getCurrentUrl()} called successfully","info"

  keyElements = ['.order_list','#back']
  for elem in keyElements
    @test.assertExists elem, "- The [#{elem}] exists"


casper.thenClick '.order_list li a', ->

  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"
  @echo "Erstes listen-element angeklickt","info"
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"

  @echo "#{@getCurrentUrl()} called successfully","info"

  keyElements = ['input#anlagennr','input#technischerplatz','#trefferanzahl-btn','#treffer','#back','#next']
  for elem in keyElements
    @test.assertExists elem, "- The [#{elem}] exists"

  @test.assertNotExists '#mastnr'

  @test.assertField('anlagennr', '')
  @test.assertField('technischerplatz', '')

  @test.assertEval( ->
    jQuery('#treffer').html() == '-'
  , '#treffer is set to "-"')

  @test.assertEvalEquals ->
      jQuery('#anlagennr').parent().hasClass('ui-disabled')
  , false, '#anlagennr is enabled'

casper.then ->
  @evaluate ->
    jQuery('#anlagennr').val('123456789')
    jQuery('#anlagennr').trigger('keyup')
    jQuery('#anlagennr').trigger('click')

  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"
  @echo "[#anlagennr]: 123456789","info"
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"

casper.then ->
  @test.assertEvalEquals ->
    jQuery('#trefferanzahl-btn').parent().hasClass('ui-disabled')
  , false, '#trefferanzahl-btn is enabled'

  @test.assertEvalEquals ->
    jQuery('#trefferanzahl-btn').parent().hasClass('ui-disabled')
  , false, '#trefferanzahl-btn is enabled'

  @test.assertEvalEquals ->
    jQuery('#anlagennr').parent().hasClass('ui-disabled')
  , false, '#anlagennr is still enabled'

  @test.assertField('anlagennr', '123456')


casper.run ->
  @log "\n===============================================================\n","info"
  @log 'finished!',"info"
  @test.done()
  @exit()
