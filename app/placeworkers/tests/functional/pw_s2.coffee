phantom.casperPath = 'tools/casperjs'
phantom.injectJs "#{phantom.casperPath}/bin/bootstrap.js"


screenname = 's2'
pixelratio = 1.5
viewport =
  width: Math.round(480 / pixelratio)
  height: Math.round(640 / pixelratio)

casper = require('casper').create
  logLevel: 'debug'
  #verbose: true
  viewportSize: viewport

pw = require('./app/placeworkers/tests/functional/pw_TestsHelper.coffee')
pw = new pw(casper, viewport, screenname)


# casper.on 'remote.message', (msg) ->
#   this.echo('remote message caught: ' + msg)


getNrOfListItems = ->
  jQuery('.order_list').find('li').length



link = 'http://localhost:8472/app.html#view?name=SearchForm2View&pid=42&objectCatId=201&assembler=&categorieId=2&searchFormNumber=2'

### Start ###
casper.start()

## ☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰
## Autocomplete should show up on every visit [BEGIN]
## Issue: https://trello.com/c/QHxdMGub/279-s2-umspannwerke-beim-ersten-aufruf-offnet-sich-vorschlag-sofort-danach-erst-beim-ersten-buchstaben

# casper.waitFor
#   (check = ->
#     @evaluate ->
#       document.querySelectorAll("ul.your-list li").length > 2
#   ), (then = -> # step to execute when check() is ok
#     @captureSelector "yoursitelist.png", "ul.your-list"
#     return
#   ), timeout = -> # step to execute if check has failed
#     @echo("I can't haz my screenshot.").exit()
#     return

casper.thenOpen 'http://localhost:8472/app.html#view?name=ObjectTypeCategoriesView&pid=203&categorieId=2', ->
  @echo "checking elements of #{@getCurrentUrl()}:\n","info"
  @click '#ojectkat_201'

casper.wait 1000, ->
  @test.assertVisible '.autocomplete-suggestions'

casper.thenClick '#back', ->
  @test.assertNotVisible '.autocomplete-suggestions'
  @click '#ojectkat_201'

casper.wait 1000, ->
  @test.assertVisible '#umspannwerk'
  @test.assertVisible '.autocomplete-suggestions', 'Autocomplete shows up on the second visit'

## Autocomplete should show up on every visit [END]
## ☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰



## ☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰
## Enter should lead to next page [BEGIN]
## Issue: https://trello.com/c/qUuP5dWj/282-s2-umspannwerk-umspannwerk-ist-ausgewahlt-hardwaretaste-enter-lad-seite-neu-mit-leerem-umspannwerk

casper.thenOpen 'http://localhost:8472/app.html#view?name=SearchForm2View&pid=203&objectCatId=201&assembler=&categorieId=2&searchFormNumber=2', ->
  @echo "checking elements of #{@getCurrentUrl()}:\n","info"

casper.wait 1000, ->
  @test.assertVisible '.autocomplete-suggestions'
  @click '.autocomplete-suggestions .autocomplete-suggestion'
  @click('#umspannwerk')
  @page.sendEvent("keypress", this.page.event.key.Enter);

casper.then ->
  @test.assertNotVisible '#umspannwerk'
  @test.assertVisible ".order_list"
  pw.makeCapture()

## Enter should lead to next page [END]
## ☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰☰



### Check the existence of all the Key-Elements on the Page ###
casper.thenOpen link, ->
  @echo "checking elements of #{@getCurrentUrl()}:\n","info"

  keyElements = ['#umspannwerk','#back']
  for elem in keyElements
    @test.assertExists elem, "- The [#{elem}] exists"

### Wait for autocomplete-suggestions ###
casper.wait 700, ->
  @fill 'form', {
    'umspannwerk': '123'
  }, false
  @evaluate pw.keyup, ['#umspannwerk']

casper.then ->
  @fill 'form', {
    'umspannwerk': 'Anlagenteil UW 12345'
  }, false
  @evaluate pw.keyup, ['#umspannwerk']

casper.then ->
  @test.assertEval( ->
    jQuery('#treffer').html() != '-'
  , 'Trefferanzahl wurde gesetzt')

### Wähle ein umspannwerk aus ###

casper.thenClick '.autocomplete-suggestions .autocomplete-suggestion', ->

  @test.assertNotVisible '.autocomplete-suggestions'

  @test.assert @evaluate( ->
      jQuery('input[name=anlagennr]').val() != ''
  ), 'anlagennr is filled with something'

  @test.assertEval( ->
    jQuery('#treffer').html() != '-'
  , 'Trefferanzahl wurde gesetzt')

  @test.assertEvalEquals ->
    jQuery('#next').parent().hasClass('ui-disabled')
  , false, '#next is enabled'

  pw.makeCapture()

casper.thenClick '#next', ->
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"
  @echo "Clicked #next","info"
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"

casper.thenClick '#back', ->
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"
  @echo "Clicked #back","info"
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"


### Teste ob die Session-Daten wieder eingetragen wurden ###

casper.wait 500, ->
  @test.assertField 'umspannwerk', 'Anlagenteil UW 12345', '#umspannwerk enthält "Anlagenteil UW 12345"'

  @test.assertEval (->
    jQuery('#treffer').html() != '-')
  , 'treffer wurden gesetzt'


### Teste ob die suggenstions korrekt dargestellt werden ###

casper.then ->
  pw.assertExists ['.autocomplete-suggestions']
  @test.assertNotVisible '.autocomplete-suggestions'

  @fill 'form', {
    'umspannwerk': 'Anlagenteil UW 1234'
  }, false

  @test.assertEval ->
    jQuery('.autocomplete-suggestions').css('width').replace('px', '') > 50
  , 'Autocomplete ist breiter als 50px'


# Test startAutocompreteInterval()
casper.then ->

  @evaluate ->
    jQuery('input:first').val('an')
    jQuery('input:first').trigger('keyup')

casper.wait 700, ->
  @test.assertEval ->
    jQuery('.autocomplete-suggestions').css('display') == 'block'
  , 'Autocomplete ist sichtbar'
  pw.makeCapture()
# Test startAutocompreteInterval()

casper.thenClick '#next', ->
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"
  @echo "Clicked #next","info"
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"

casper.wait 200, ->
  @echo "Clicked [.order_list li]","info"
  @test.assertEval (->
    !jQuery('[data-role="content"]').hasClass('ui-disabled'))
  , 'content is enabled'

  @evaluate ->
    jQuery('.order_list li:eq(3)').click()

casper.wait 100, ->
  @echo "Clicked [.order_list li]","info"
  @test.assertEval (->
    !jQuery('[data-role="content"]').hasClass('ui-disabled'))
  , 'content is enabled'

  @evaluate ->
    jQuery('.order_list li:eq(3)').click()

casper.wait 50, ->
  @echo "Clicked [.order_list li]","info"
  @test.assertEval (->
    !jQuery('[data-role="content"]').hasClass('ui-disabled'))
  , 'content is enabled'

  @evaluate ->
    jQuery('.order_list li:eq(3)').click()

casper.wait 30, ->
  @echo "Clicked [.order_list li]","info"
  @test.assertEval (->
    !jQuery('[data-role="content"]').hasClass('ui-disabled'))
  , 'content is enabled'



casper.run ->
  @log 'finished!',"info"
  @test.done()
  @exit()
