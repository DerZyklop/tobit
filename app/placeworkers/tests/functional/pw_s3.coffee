phantom.casperPath = 'tools/casperjs'
phantom.injectJs "#{phantom.casperPath}/bin/bootstrap.js"

screenname = 's3'
pixelratio = 1.5
viewport =
  width: Math.round(480 / pixelratio)
  height: Math.round(640 / pixelratio)

casper = require('casper').create
  logLevel: 'debug'
  #verbose: true
  viewportSize: viewport

pw = require('./app/placeworkers/tests/functional/pw_TestsHelper.coffee')
pw = new pw(casper, viewport, screenname)



getNrOfListItems = ->
  jQuery('.order_list').find('li').length



link = 'http://localhost:8472/app.html#view?name=SearchForm3View&pid=42&objectCatId=301&assembler=MuN&categorieId=1&searchFormNumber=3'

### Start ###
casper.start()

### Check the existence of all the Key-Elements on the Page ###
casper.thenOpen link, ->
  @echo "checking elements of #{@getCurrentUrl()}:\n","info"

  keyElements = ['#anlagennr','#technischerplatz','#trefferanzahl-btn','#back']
  for elem in keyElements
    @test.assertExists elem, "- The [#{elem}] exists"
  pw.makeCapture()

casper.then ->
  ### New entry with too much chars ###
  pw.makeCapture()
  @fill 'form', {
    'anlagennr': '1234567'
  }, false
  @evaluate pw.keyup, ['#anlagennr']
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"
  @echo "Entered 1234567 into #anlagennr","info"
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"
  pw.makeCapture()

casper.then ->
  pw.makeCapture()

  ### Should be shortened by one char ###
  @test.assertField 'anlagennr', '123456', 'Inhalt von #anlagennr wurde auf 6 Zeichen gekürzt'
  @test.assertEvalEquals pw.assertTrefferBtnIsDisabled, false, '#trefferanzahl-btn ist enabled'

  @fill 'form', {
    'technischerplatz': 'r43t5hterzuk5tree5678654'
  }, false
  @evaluate pw.keyup, ['#technischerplatz']

  pw.makeCapture()

  pw.assertElementsStatus [{
      selector: '#next',
      enabled: false
    }]

casper.then ->
  keyElements = ['#anlagennr','#technischerplatz','#back']
  for elem in keyElements
    @test.assertExists elem, "- The [#{elem}] exists"

casper.thenClick '#trefferanzahl-btn', ->
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"
  @echo "Clicked #trefferanzahl-btn","info"
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"

casper.wait 200, ->
  @test.assertEval (->
    jQuery('#treffer').html() != '-')
  , 'treffer wurden gesetzt'
  pw.assertElementsStatus [{
      selector: '#next',
      enabled: true
    }]

casper.thenClick '#next', ->
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"
  @echo "Clicked #next","info"
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"

casper.thenClick '#back', ->
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"
  @echo "Clicked #back","info"
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"

### Teste ob die Session-Daten wieder eingetragen wurden ###

casper.then ->
  @test.assertField 'technischerplatz', 'r43t5hterzuk5tree5678654', '#technischerplatz enthält "r43t5hterzuk5tree5678654"'
  @test.assertField 'anlagennr', '123456', 'Inhalt von #anlagennr ist immernoch 123456'
  pw.assertElementsStatus [{
      selector: '#next',
      enabled: true
    }]

  @test.assertEvalEquals pw.assertTrefferBtnIsDisabled, true, '#trefferanzahl-btn ist disabled'

  ############### fwefewfewfwefewf HIER WIRD KEIN EVENT AUSGELÖST
  @fill 'form', {
    'technischerplatz': 'r43'
  }, false

  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"
  @echo "Filled #technischerplatz with r43","info"
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"

  @evaluate pw.keyup, ['#technischerplatz']

  @test.assertEvalEquals pw.assertTrefferBtnIsDisabled, false, '#trefferanzahl-btn ist enabled'

casper.thenClick '#trefferanzahl-btn', ->
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"
  @echo "Clicked #trefferanzahl-btn","info"
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"

casper.wait 200, ->
  @test.assertEval (->
    jQuery('#treffer').html() != '-')
  , 'treffer wurden gesetzt'


### Teste ob die Liste richtig dargestellt wird ###
casper.thenClick '#next', ->
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"
  @echo "Clicked #next","info"
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"

casper.wait 200, ->
  @echo "Clicked [.order_list li]","info"
  @test.assertEval (->
    !jQuery('[data-role="content"]').hasClass('ui-disabled'))
  , 'content is enabled'

  @evaluate ->
    jQuery('.order_list li:eq(3)').click()

casper.wait 100, ->
  @echo "Clicked [.order_list li]","info"
  @test.assertEval (->
    !jQuery('[data-role="content"]').hasClass('ui-disabled'))
  , 'content is enabled'

  @evaluate ->
    jQuery('.order_list li:eq(3)').click()

casper.wait 50, ->
  @echo "Clicked [.order_list li]","info"
  @test.assertEval (->
    !jQuery('[data-role="content"]').hasClass('ui-disabled'))
  , 'content is enabled'

  @evaluate ->
    jQuery('.order_list li:eq(3)').click()

casper.wait 30, ->
  @echo "Clicked [.order_list li]","info"
  @test.assertEval (->
    !jQuery('[data-role="content"]').hasClass('ui-disabled'))
  , 'content is enabled'

casper.run ->
  @log 'finished!',"info"
  @test.done()
  @exit()
