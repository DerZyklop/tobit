class PwTestsHelper

  constructor: (casper, viewport, screenname) ->
    @casper = casper
    @screenname = screenname
    @captureSize =
      height: viewport.height
      width: viewport.width
      top: 0
      left: 0

    @makeCapture = makeCapture
    @keyup = keyup
    @checkSuggestions = checkSuggestions
    @getSuggestionsAmount = getSuggestionsAmount
    @assertExists = assertExists
    @elementIsEnabled = elementIsEnabled
    @assertElementsStatus = assertElementsStatus
    @assertTrefferBtnIsDisabled = assertTrefferBtnIsDisabled

  makeCapture = (->
    i = 0
    return ->
      path = 'app/placeworkers/tests/screenshots/pw_'+@captureSize.width+'x'+@captureSize.height+'_'+@screenname+'_screen'+i+'.png'
      @casper.echo('Creating capture: '+path)
      @casper.capture(path, @captureSize)
      i++
  )()


  keyup = (selector) ->
    jQuery(selector).trigger('keyup')


  checkSuggestions = ->
    @casper.test.assertEvalEquals ->
      jQuery('.autocomplete-suggestions').length
    ,1 , "There is just one [.autocomplete-suggestions]"
    @assertExists ['.autocomplete-suggestions']

  getSuggestionsAmount = ->
    jQuery('.autocomplete-suggestions').children().length

  assertExists = (elements) ->
    for elem in elements
      @casper.test.assertExists elem, "- The [#{elem}] exists"

  elementIsEnabled = (selector, status) ->
    status == jQuery(selector).is(':enabled')

  assertElementsStatus = (elements) ->
    for elem in elements
      @casper.test.assertExists elem.selector, "- The [#{elem.selector}] exists"
      if elem.enabled then word = 'enabled' else word = 'disabled'
      @casper.test.assertEval elementIsEnabled, "[#{elem.selector}] ist #{word}", [elem.selector, elem.enabled]

  assertTrefferBtnIsDisabled = ->
    jQuery('#trefferanzahl-btn').parent().hasClass('ui-disabled')


module.exports = PwTestsHelper
