phantom.casperPath = 'tools/casperjs'
phantom.injectJs "#{phantom.casperPath}/bin/bootstrap.js"

screenname = 's3'
pixelratio = 1.5
viewport =
  width: Math.round(480 / pixelratio)
  height: Math.round(640 / pixelratio)

casper = require('casper').create
  logLevel: 'debug'
  #verbose: true
  viewportSize: viewport

pw = require('./app/placeworkers/tests/functional/pw_TestsHelper.coffee')
pw = new pw(casper, viewport, screenname)



getNrOfListItems = ->
  jQuery('.order_list').find('li').length



link = 'http://localhost:8472/app.html#view?name=SearchForm3View&pid=42&objectCatId=301&assembler=2&categorieId=1&searchFormNumber=3'

### Start ###
casper.start()

### Check the existence of all the Key-Elements on the Page ###
casper.thenOpen link, ->
  @echo "checking elements of #{@getCurrentUrl()}:\n","info"

  keyElements = ['#anlagennr','#technischerplatz','#trefferanzahl-btn','#back']
  for elem in keyElements
    @test.assertExists elem, "- The [#{elem}] exists"
  pw.makeCapture()


casper.wait 200, ->
  @echo "The site needs some time (200ms) to set all event-listeners","info"

  ### New entry with too much chars ###
  @fill 'form', {
    'anlagennr': '1234567'
  }, false
  @evaluate pw.keyup, ['#anlagennr']


  ### New entry with too much chars ###
  @fill 'form', {
    'mastnr': '1234567'
  }, false
  @evaluate pw.keyup, ['#mastnr']

### Should be shortened by one char ###
casper.then ->
  @test.assertField 'anlagennr', '1234', 'Inhalt von #anlagennr wurde auf 4 Zeichen gekürzt'
  @test.assertField 'mastnr', '12345', 'Inhalt von #mastnr wurde auf 5 Zeichen gekürzt'

  @fill 'form', {
    'technischerplatz': 'r43t5hterzuk5tree5678654'
  }, false
  @evaluate pw.keyup, ['#technischerplatz']
  pw.makeCapture()

casper.thenClick '#next', ->
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"
  @echo "Clicked #next","info"
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"

casper.then ->
  keyElements = ['#anlagennr','#technischerplatz','#back']
  pw.makeCapture()
  for elem in keyElements
    @test.assertExists elem, "- The [#{elem}] exists"

casper.thenClick '#trefferanzahl-btn', ->
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"
  @echo "Clicked #trefferanzahl-btn","info"
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"

casper.wait 200, ->
  @test.assertEval (->
    jQuery('#treffer').html() != '-')
  , 'treffer wurden gesetzt'

casper.thenClick '#next', ->
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"
  @echo "Clicked #next","info"
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"

casper.then ->
  pw.makeCapture()
  @test.assertUrlMatch(/mastNumber=/, 'URL enthält "mastNumber"')

casper.thenClick '#back', ->
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"
  @echo "Clicked #back","info"
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"


### Teste ob die Session-Daten wieder eingetragen wurden ###

casper.then ->
  @test.assertField 'technischerplatz', 'r43t5hterzuk5tree5678654', '#technischerplatz enthält "r43t5hterzuk5tree5678654"'
  @test.assertField 'anlagennr', '1234', '#anlagennr enthält "1234"'
  @test.assertField 'mastnr', '12345', 'Inhalt von #mastnr wurde auf 5 Zeichen gekürzt'

  @test.assertEval (->
    jQuery('#treffer').html() != '-')
  , 'treffer wurden gesetzt'


casper.then ->
  ### Remove anlagennr-value ###
  @fill 'form', {
    'anlagennr': ''
  }, false
  @evaluate pw.keyup, ['#anlagennr']

  @echo "Inhalt von #anlagennr wurde vom user gelöscht","info"
  @test.assertField 'mastnr', '', 'Inhalt von #mastnr wurde von der app gelöscht'


### Teste ob die Liste richtig dargestellt wird ###
casper.thenClick '#next', ->
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"
  @echo "Clicked #next","info"
  @echo "\n––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n","info"

casper.wait 200, ->
  @echo "Clicked [.order_list li]","info"
  @test.assertEval (->
    !jQuery('[data-role="content"]').hasClass('ui-disabled'))
  , 'content is enabled'

  @evaluate ->
    jQuery('.order_list li:eq(3)').click()

casper.wait 100, ->
  @echo "Clicked [.order_list li]","info"
  @test.assertEval (->
    !jQuery('[data-role="content"]').hasClass('ui-disabled'))
  , 'content is enabled'

  @evaluate ->
    jQuery('.order_list li:eq(3)').click()

casper.wait 50, ->
  @echo "Clicked [.order_list li]","info"
  @test.assertEval (->
    !jQuery('[data-role="content"]').hasClass('ui-disabled'))
  , 'content is enabled'

  @evaluate ->
    jQuery('.order_list li:eq(3)').click()

casper.wait 30, ->
  @echo "Clicked [.order_list li]","info"
  @test.assertEval (->
    !jQuery('[data-role="content"]').hasClass('ui-disabled'))
  , 'content is enabled'

casper.run ->
  @log 'finished!',"info"
  @test.done()
  @exit()
