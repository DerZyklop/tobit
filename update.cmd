:: Make nicer output
@echo off

:: Clone tobit if not already cloned
cd C:\workspace\
git clone https://bitbucket.org/DerZyklop/tobit.git

:: Update tobit
cd C:\workspace\tobit
git pull

:: Delete all files and folders in WFM2-UI\app\placeworkers
FOR /D %%i IN (C:\workspace\WFM2-UI\app\placeworkers\*) DO RD /S /Q %%i
DEL /Q C:\workspace\WFM2-UI\app\placeworkers\*.*

:: Copy updated tobit content to WFM2-UI\app\placeworkers
xcopy /E C:\workspace\tobit\app\placeworkers C:\workspace\WFM2-UI\app\placeworkers

:: Do bit‘s update foo
cd C:\workspace\WFM2-UI\
packageload.sup.cmd
integrate.cmd

echo Enbw update fertig
pause
